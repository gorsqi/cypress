const path = require('path')
const fs = require('fs-extra')
const cucumber = require('cypress-cucumber-preprocessor').default
module.exports = (on, config) => {

  //Cucumber
  on('file:preprocessor', cucumber())

  //Browser launch
  on('before:browser:launch', (browser = {}, args) => {
    if (browser.name === 'chrome') {
      return args
    }
    if (browser.name === 'electron') {
      args['fullscreen'] = false
      return args
    }
  })


  function processConfigName(on, config) {

    const file = config.env.name || "default"
    return getConfigFile(file).then(function (file) {
      return file;
    })
  }

  function getConfigFile(file) {
        const pathToConfigFile = path.resolve('cypress', 'config', `${file}.json`)
        return fs.readJson(pathToConfigFile)
      }
  return processConfigName(on, config);
  }

  