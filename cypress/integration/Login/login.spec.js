import user from '../../fixtures/user'
import alert from '../../fixtures/alert'

describe("Login", () => {
    beforeEach(() => cy.visit("/", { "failOnStatusCode": false }))

    it("01-Inserindo e-mail inválido", () => {
        cy.get('#email').type(user.emailFake + '{enter}')

        cy.contains(alert.msgErro.ME002).should("exist")
    })


    it("02-Informo Email e Senha inválidos", () => {
        cy.get('#email').type(user.email)
        cy.get('#password').type(user.passwordFake)
        cy.get('#btn_login').click()

        cy.contains(alert.msgErro.ME003).should("be.visible")
    })


    it("03-Visualizar senha", () => {
        cy.get('#password').type(user.passwordFake)
        cy.get('.fa-eye').click()

        cy.get('input[name="password"]').wrap({ type: "text" }).its("type").should("eq", "text")
    })


    it("04-Esqueceu senha", () => {
        const forgotPass = "/login/esqueceu-senha"

        cy.get('.lost-pass').should("be.visible")
            .should("be.visible")
            .and("to.have.prop", "href", `${Cypress.config("baseUrl")}${forgotPass}`)
    })


    it("05-Não possui conta, esta visível?", () => {
        const step1 = "/cadastro/etapa-1"

        cy.get('.info-before-login').should("be.visible")

        cy.contains("Cadastre-se grátis.")
            .should("be.visible")
            .and("to.have.prop", "href", `${Cypress.config("baseUrl")}${step1}`)
    })


    it("06-Login com sucesso", () => {
        cy.loginSuccess()

        cy.get("#style-3").should("be.visible")
    })
})