import user from '../../fixtures/user'
import alert from '../../fixtures/alert'


describe("ONBOARD", () => {
    beforeEach(() => {
        cy.deleteUser()
        cy.visit("/cadastro/etapa-1", { "failOnStatusCode": false })
    })

    context("EMAIL", () => {
        it("01-Campo Obrigatório", () => {
            cy.get("[type='submit']").click()

            cy.get(".text-danger")
                .should("contain", alert.msgErro.ME001) //"Campo obrigatório."
        })

        it("02-Inserir E-mail Inválido", () => {
            cy.get("#email").type(user.emailFake)
            cy.get("[type='submit']").click()

            cy.get(".text-danger")
                .should("contain", alert.msgErro.ME002) // "E-mail inválido."

        })

        it("03-Inserir E-mail Existente", () => {
            cy.get("#email").type(Cypress.env('emailExist'))
            cy.get("[type='submit']").click()

            cy.get(".alert")
                .should("contain", alert.msgErro.ME005) //" E-mail já cadastrado, clique aqui para efetuar seu login."
        })
    })


    context("CPF", () => {
        it("01-Insere CPF", () => {
            cy.goToStep2(user)
            
            cy.get("#identifier").type(Cypress.env('cpf'))
            cy.get(".btn").click()

            cy.get(".text-danger").its("length").should("eq", 2)
        })

        it("02-Data de Nascimento inválida", () => {
            cy.goToStep2(user)
            cy.userDataOld()

            cy.get(".text-danger")
                .should("contain", alert.msgErro.ME013)
        })

        it("03-CPF ou Data de Nascimento não cadastrado na receita", () => {
            cy.goToStep2(user)
            cy.userData()

            cy.get('.col-md-12 > [style=""]')
                .should("contain", alert.msgErro.ME007)
        })
    })

    context("CNPJ", () => {
        it("01-CNPJ Inválido", () => {
            cy.goToStep2(user)
            cy.get("#identifier").type(user.cnpjFake)
            cy.get('#state').select(user.estados.amazonas)

            cy.get('.text-danger')
                .should("contain", alert.msgErro.ME008)
        })
    })

    context("ALL STEPS", () => {
        it("01-Cria um novo usuário", () => {
            cy.goToStep2(user)
            cy.assertStep2()

            cy.goToStep3(user)
            cy.assertStep3()

            cy.goToStep4(user)
            cy.assertStep4()

            cy.goToLogin()
            cy.get('.free-days-msg').should("contain", "Você tem 30 dias grátis")
        })
    })
})