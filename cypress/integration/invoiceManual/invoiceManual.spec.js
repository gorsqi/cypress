import user from '../../fixtures/user'
import alert from '../../fixtures/alert'

describe("Invoice Manual", () => {
    beforeEach(() => {
        cy.deleteInvoice()
    })

    context("INSERT MANUAL", () => {
        it("01-Insere Boleto Manual", () => {
            cy.loginSuccess()
            
            cy.invoiceManual(Cypress.env('barCode'), user.recipient, 1)

            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN023)
        })
    })
})