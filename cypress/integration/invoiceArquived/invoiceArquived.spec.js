import user from '../../fixtures/user'
import alert from '../../fixtures/alert'

describe("Invoice Manual", () => {
    beforeEach(() => {
        cy.loginSuccess()
        cy.deleteInvoice()
    })

    context("ARQUIVED", () => {
        it("01-Testando a funcionalidade de arquivar um boleto.", () => {
            cy.invoiceManual(Cypress.env('barCodeRenner'), user.recipientRenner, 'RENNER')

            cy.arquivedInvoice('RENNER')
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN028)

            cy.deleteAllInvoicesArquived()
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN029)
        })

        it("02-Testando a funcionalidade de desarquivar um boleto.", () => {
            cy.invoiceManual(Cypress.env('barCode'), user.recipient, 'EST UNIF')
            cy.arquivedInvoice('EST')

            cy.get("#btn_arquivo").click()
            cy.get(':nth-child(2) > .VueTables__heading > .form-check > .container-check > .checkmark').click()
            cy.get('.btn_restore-selected > .fa').click()

            cy.get(".panel-confirmation").should("exist")

            cy.get(".btn-success").click()
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN030)

            cy.deleteAllInvoices()
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN029)
        })

        it("03-Testando a deleção de um boleto arquivado.", () => {
            cy.invoiceManual(Cypress.env('barCodeBra'), user.recipientBra, 'BRADESCO EST UNIF')
            cy.arquivedInvoice('BRADESCO EST UNIF')
            cy.get("#btn_arquivo").click()

            cy.get(':nth-child(2) > .VueTables__heading > .form-check > .container-check > .checkmark').click()
            cy.get('.my-invoices-group > .table-filter > :nth-child(1) > .fa').click()

            cy.get(".panel-confirmation").should("exist")

            cy.get(".btn-success").click()
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN030)
        

            cy.deleteAllInvoices()
            cy.get('.iziToast-texts').should("contain", alert.msgBusines.MN029)
        })
    })
})