import user from '../../fixtures/user'

Cypress.Commands.add("userData", () => {
    cy.get("#identifier").type(Cypress.env('cpf'))
    cy.get('#state').select(user.estados.acre)
    cy.get("#birth_date").type(user.birthDate)

    cy.get(".btn").click()
})

Cypress.Commands.add("userDataOld", () => {
    cy.get("#identifier").type(Cypress.env('cpf'))
    cy.get('#state').select(user.estados.bahia)
    cy.get("#birth_date").type(user.birthDateOld)
    
    cy.get(".btn").click()
})

Cypress.Commands.add("loginSuccess", () => {
    cy.visit("/")
    cy.get('#email').type(Cypress.env('emailExist'))
    cy.get('#password').type(Cypress.env('password'))
    cy.get('#btn_login').click()

})

Cypress.Commands.add("logout", () => {
    cy.get('.navbar-right-menu > .nav > #btn_dropdown > .dropdown-toggle > .notification').click()
    cy.get('.dropdown-menu > :nth-child(6) > a').click()
    cy.get('.btn-success').click()
})