import 'cypress-file-upload'

Cypress.Commands.add("getPIN", () => {
    cy.request({
        method: 'GET',
        url: "https://api-teste.billingdelivery.com/alpha/alpha/users/02776705000120/get-security-code",
        headers: {
            'Authorization': 'Basic dGlsaXg6cGFzc3dvcmQ='
        }
    }).then(response => {
        cy.get('#identifier').type(response.body.security_code)
    })
})

Cypress.Commands.add("deleteUser", () => {
    cy.request({
        method: 'DELETE',
        url: "https://1rtjq7wgih.execute-api.us-east-1.amazonaws.com/dev/user",
        headers: {
            'Authorization': 'eGlsaXRyZW1vdmV1c2VyLnBhc3MwIyM=',
            'Content-Type': 'application/json'
        },
        body: {
            "remove_billing": true,
            "email": "ytxytrqbte@mail.com",
            "identifier": "02776705000120",
            "stage": "qa"
        }
    })
})

// Cypress.Commands.add("deleteUserCognito", () => {
//     cy.request({
//         method: 'DELETE',
//         url: "https://api-teste.billingdelivery.com/alpha/accounts/02776705000120",
//         headers: {
//             'Authorization': 'dGlsaXg6cGFzc3dvcmQ='
//         }
//     })
// })



Cypress.Commands.add("deleteInvoice", () => {
    cy.request({
        method: 'DELETE',
        url: "https://api-teste.billingdelivery.com/alpha/alpha/invoices/08958961686",
        headers: {
            'Authorization': 'Basic dGlsaXg6cGFzc3dvcmQ='
        }
    })
})

Cypress.Commands.add("uploadFile", () => {
    cy.fixture('invoice.pdf').then(fileContent => {
        cy.get("#btn_attach-file").upload({
            fileContent,
            fileName: 'invoice.pdf',
            mimeType: 'application/pdf'
        },
            {
                uploadType: 'pdf'
            }
        )
    })
})


Cypress.Commands.add("invoiceManual", (barcode, user, company) => {
    cy.get('.sidebar > #style-3 > .nav > :nth-child(1) > .has-tooltip > .tlx').click()

    cy.get(".in").should("exist")

    cy.get("#barcode", {timeout: 5000}).type(barcode)
    cy.get('.form-group > .v-autocomplete > .v-autocomplete-input-group > .v-autocomplete-input').type(user)
    cy.get(".v-autocomplete-list").contains(company).click()
    cy.get("#btn_attach-file", {timeout: 5000}).click()
    
    cy.uploadFile()
    cy.get("#btn_confirm-payment").click()
})

Cypress.Commands.add("arquivedInvoice", (beneficiario) => {
    cy.get("#btn_boletos").click()

    cy.get(".col-md-12").should("exist")

    cy.get('.tablesaas').find('tr').contains(beneficiario).parent('td').click()
    cy.get('.fa-archive').click()

    cy.get(".panel-confirmation").should("exist")

    cy.get(".btn-success").click()
})

Cypress.Commands.add("deleteAllInvoices", () => {
    cy.get("#btn_boletos").click()
    cy.get('[title=""] > .VueTables__heading > .form-check > .container-check > .checkmark').click()
    cy.get('.my-invoices-group > .table-filter > :nth-child(1) > .fa').click()

    cy.get(".panel-confirmation").should("exist")
    
    cy.get('.btn-default').click()
})

Cypress.Commands.add("deleteAllInvoicesArquived", () => {
    cy.get("#btn_arquivo").click()
    cy.get(':nth-child(2) > .VueTables__heading > .form-check > .container-check > .checkmark').click()
    cy.get('.btn_filter-invoices > .fa').click()

    cy.get(".panel-confirmation").should("exist")

    cy.get(".btn-success").click()
})