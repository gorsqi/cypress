Cypress.Commands.add("goToStep2", user => {
    cy.get("#email").type(user.email)
    cy.get("[type='submit']").click()
})

Cypress.Commands.add("goToStep3", user => {
    cy.get("#identifier").type(Cypress.env('cnpj'))
    cy.get('#state').select(user.estados.amazonas)
    cy.get(".btn").click()
})

Cypress.Commands.add("goToStep4", user => {
    cy.get('#name').type(user.firstName)
    cy.get('#family_name').type(user.familyName)
    cy.get('#phone_number').type(Cypress.env('phoneNumber'))
    cy.get('.VuePassword__Input > .form-control').type(Cypress.env('password'))
    cy.get('#confirm_password').type(Cypress.env('password'))
    cy.get('.checkmark').click()
    cy.get('.btn').click()
})

Cypress.Commands.add("goToLogin", user => {
    cy.getPIN()
    cy.get(".btn").click()
})