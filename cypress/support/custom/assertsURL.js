Cypress.Commands.add("assertStep1", () => {
    cy.url().should(
        "be.equal",
        `${Cypress.config().baseUrl}/cadastro/etapa-1`)
})

Cypress.Commands.add("assertStep2", () => {
    cy.url().should(
        "be.equal",
        `${Cypress.config().baseUrl}/cadastro/etapa-2`)
})

Cypress.Commands.add("assertStep3", () => {
    cy.url().should(
        "be.equal",
        `${Cypress.config().baseUrl}/cadastro/etapa-3`)
})

Cypress.Commands.add("assertStep4", () => {
    cy.url().should(
        "be.equal",
        `${Cypress.config().baseUrl}/cadastro/etapa-4`)
})